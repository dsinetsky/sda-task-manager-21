package ru.t1.dsinetsky.tm.repository;

import ru.t1.dsinetsky.tm.api.repository.ITaskRepository;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findTasksByProjectId(final String userId, final String projectId) {
        return returnAll(userId)
                .stream()
                .filter(m -> projectId.equals(m.getProjectId()))
                .collect(Collectors.toList());
    }

    @Override
    public Task create(final String userId, final String name) {
        if (isUserIdEmpty(userId)) return null;
        if (name == null || name.isEmpty()) return null;
        return add(userId, new Task(name));
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        if (isUserIdEmpty(userId)) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return add(userId, new Task(name, description));
    }

}
