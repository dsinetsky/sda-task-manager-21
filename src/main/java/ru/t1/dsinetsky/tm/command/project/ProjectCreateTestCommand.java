package ru.t1.dsinetsky.tm.command.project;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;

public final class ProjectCreateTestCommand extends AbstractProjectCommand {

    public static final String NAME = TerminalConst.CMD_CREATE_TEST_PROJECTS;

    public static final String DESCRIPTION = "Create 10 projects for tests";

    @Deprecated
    public void execute() throws GeneralException {
        System.out.println("Method is deprecated!");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
