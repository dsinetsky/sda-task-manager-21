package ru.t1.dsinetsky.tm.command.task;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;

public final class TaskCreateTestCommand extends AbstractTaskCommand {

    public static final String NAME = TerminalConst.CMD_CREATE_TEST_TASKS;

    public static final String DESCRIPTION = "Create 10 tasks for tests";

    @Deprecated
    public void execute() throws GeneralException {
        System.out.println("Method is deprecated!");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
