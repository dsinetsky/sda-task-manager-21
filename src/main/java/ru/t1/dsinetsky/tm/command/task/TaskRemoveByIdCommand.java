package ru.t1.dsinetsky.tm.command.task;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    public static final String NAME = TerminalConst.CMD_REMOVE_TASK_BY_ID;

    public static final String DESCRIPTION = "Removes task (if any) found by id";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter id of task:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getTaskService().removeById(userId, id);
        System.out.println("Task successfully removed!");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
