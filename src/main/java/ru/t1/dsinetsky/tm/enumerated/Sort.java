package ru.t1.dsinetsky.tm.enumerated;

import ru.t1.dsinetsky.tm.comparator.CreatedComparator;
import ru.t1.dsinetsky.tm.comparator.NameComparator;
import ru.t1.dsinetsky.tm.comparator.StatusComparator;
import ru.t1.dsinetsky.tm.exception.system.InvalidSortTypeException;

import java.util.Comparator;

public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE);

    private final String displayName;

    private final Comparator comparator;

    Sort(final String displayName, final Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public static Sort toSort(final String value) throws InvalidSortTypeException {
        if (value == null || value.isEmpty()) return null;
        for (final Sort sort : values()) {
            if (value.equals(sort.name())) return sort;
        }
        throw new InvalidSortTypeException();
    }

    public static String getSortList() {
        StringBuilder sortList = new StringBuilder();
        for (final Sort sort : values()) {
            sortList.append(" - ").append(sort.name()).append("\n");
        }
        return sortList.toString();
    }

    public Comparator getComparator() {
        return comparator;
    }
}
