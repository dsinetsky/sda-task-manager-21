package ru.t1.dsinetsky.tm.exception.entity;

public final class EntityNotFoundException extends GeneralEntityException {

    public EntityNotFoundException(final String modelName) {
        super(modelName + " not found!");
    }

}
