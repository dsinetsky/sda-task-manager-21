package ru.t1.dsinetsky.tm.exception.field;

public final class EntityIdIsEmptyException extends GeneralFieldException {

    public EntityIdIsEmptyException(final String modelName) {
        super(modelName + " id cannot be empty!");
    }

}
