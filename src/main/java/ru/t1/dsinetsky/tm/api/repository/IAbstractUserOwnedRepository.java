package ru.t1.dsinetsky.tm.api.repository;

import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IAbstractUserOwnedRepository<M extends AbstractModel> extends IAbstractRepository<M> {

    List<M> returnAll(String userId) throws GeneralException;

    List<M> returnAll(String userId, Comparator comparator) throws GeneralException;

    void clear(String userId) throws GeneralException;

    M findById(String userId, String id) throws GeneralException;

    M findByIndex(String userId, int index) throws GeneralException;

    M removeById(String userId, String id) throws GeneralException;

    M removeByIndex(String userId, int index) throws GeneralException;

    boolean existsById(String userId, String id) throws GeneralException;

    int getSize(String userId) throws GeneralException;

    M add(String userId, M model) throws GeneralException;

}
