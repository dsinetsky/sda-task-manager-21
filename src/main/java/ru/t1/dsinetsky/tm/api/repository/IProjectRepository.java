package ru.t1.dsinetsky.tm.api.repository;

import ru.t1.dsinetsky.tm.model.Project;

public interface IProjectRepository extends IAbstractUserOwnedRepository<Project> {

    Project create(String userId, String name);

    Project create(String userId, String name, String desc);

}
