package ru.t1.dsinetsky.tm.api.service;

public interface ILoggerService {

    void info(String message);

    void debug(String message);

    void error(Exception e);

    void command(String message);

}
