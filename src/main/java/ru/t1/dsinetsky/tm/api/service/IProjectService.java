package ru.t1.dsinetsky.tm.api.service;

import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Project;

public interface IProjectService extends IAbstractUserOwnedService<Project> {

    Project create(String userId, String name) throws GeneralException;

    Project create(String userId, String name, String desc) throws GeneralException;

    Project updateById(String userId, String id, String name, String desc) throws GeneralException;

    Project updateByIndex(String userId, int index, String name, String desc) throws GeneralException;

    Project changeStatusById(String userId, String id, Status status) throws GeneralException;

    Project changeStatusByIndex(String userId, int index, Status status) throws GeneralException;

}
