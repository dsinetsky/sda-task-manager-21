package ru.t1.dsinetsky.tm.api.repository;

import ru.t1.dsinetsky.tm.model.User;

public interface IUserRepository extends IAbstractRepository<User> {

    User findUserByLogin(String login);

    boolean isUserLoginExist(String login);

    void removeByLogin(String login);

}
