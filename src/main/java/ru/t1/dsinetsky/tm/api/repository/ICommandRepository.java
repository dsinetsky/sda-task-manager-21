package ru.t1.dsinetsky.tm.api.repository;

import ru.t1.dsinetsky.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    Collection<AbstractCommand> getTerminalCommands();

    void add(AbstractCommand command);

    AbstractCommand getCommandByName(String commandName);

    AbstractCommand getCommandByArgument(String argument);

}
